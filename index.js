const express = require('express');
const app = express();
const http = require('http').createServer(app);
const io = require('socket.io')(http);
const dotenv = require('dotenv').config();
const open = require('open');
const Atem = require('atem');
var myAtemDevice = new Atem();

var atemStatus = {
    PROGRAM: 0,
    PREVIEW: 0
};

app.use(express.static(__dirname));

// Atem Device
myAtemDevice.on('connectionStateChange', function(state) {
	console.log('state', state);
});

myAtemDevice.on('productNameChange', function(name) {
	console.log('name', name);
});

myAtemDevice.on('connectionLost', function(state) {
	console.log('lost', state);
});

myAtemDevice.on('error', function(state) {
	console.error('error', state);
});

myAtemDevice.on('programBus', function(source) {
    atemStatus.PROGRAM = source;
    sendStatus();
	console.log('program bus changed to', source);
});
myAtemDevice.on('previewBus', function(source) {
    atemStatus.PREVIEW = source;
    sendStatus();
	console.log('program bus changed to', source);
});

myAtemDevice.ip = process.env.ATEM_IP;
myAtemDevice.connect();

// App Routing
app.get('/', (req, res) => {
    res.sendFile(__dirname + '/qrcode.html');
});
app.get('/device', (req, res) => {
    res.sendFile(__dirname + '/device.html');
});
app.get('/qrcode', (req, res) => {
    res.sendFile(__dirname + '/qrcode.html');
});
app.get('/emulator', (req, res) => {
    res.sendFile(__dirname + '/emulator.html');
});
app.get('/player', (req, res) => {
    res.sendFile(__dirname + '/player.html');
});
app.get('/getAtemStatus', (req, res) => {
    res.json(atemStatus);
});
app.get('/getGeneralSettings', (req, res) => {
    var jsonGeneralSettings = {
        ATEM_IP: process.env.ATEM_IP,
        PC_IP: process.env.PC_IP,
        PC_URL: "http://" + process.env.PC_IP + ":3000/",
        ENCODER_URL: "http://" + process.env.ENCODER_IP + ":8000/live/STREAM_NAME.flv"
    };
    res.json(jsonGeneralSettings);
});

http.listen(3000, () => {
    console.log('server in ascolto sulla porta *:3000');

    io.on('connection', (socket) => {
        console.log('client connesso');
        socket.on('disconnect', () => {
            console.log('client disconnesso');
        });

        //TODO: rimuovere...
        socket.on('emulatore', (d) => {
            atemStatus = d;
            sendStatus();
            console.log("DATA: ", d);
        });
    });

    open("http://" + process.env.PC_IP + ":3000/qrcode");
});

function sendStatus(){
    io.emit('tally', atemStatus);
}