# ATEMTallyLights

ATEMTallyLights è un'app che consente di riprodurre il meccanismo delle Tally Lights sfruttando tecnologia websocket su NodeJS. L'integrazione con la libreria Atem trovata su GitHub (https://github.com/Dev1an/Atem, [licenza MIT](https://github.com/Dev1an/Atem/blob/master/LICENSE.txt)). L'applicativo funziona su dispositivi con Windows 10, MacOS e Linux.

## Primo Utilizzo

Soltanto al primo utilizzo è necessario:
 1. Installare NodeJS scaricandolo da [qui](https://nodejs.dev/download).
 2. Scaricare l'archivio zip contenente tutti i file sorgente dell'applicativo (cliccare sul pulsante di download della repository presente in questa schermata accanto al pulsante "Clone").
 3. Rendere eseguibile il file `launch` cliccandoci sopra con il tasto destro, aprire `Proprietà`, poi `Permessi` e poi abilitare l'`esecuzione` del file.

I punti 2 e 3 vanno ripetuti ogni qualvolta che l'applicativo viene aggiornato.

## Utilizzo dell'applicativo

Terminata la fase di preparazione all'utilizzo, ogni volta che si desidera utilizzare l'applicativo si deve:

 1. Eseguire il setup dell'infrastruttura, connettendo ATEM e PC sul quale gira ATEMTallyLights nella stessa rete.
 2. Ottenere gli indirizzi IP della console ATEM e del PC (ed eventualmente del PC utilizzato per l'encoding, se si usa ([NodeRestreamer](https://gitlab.com/lucapada/noderestreamer)).
 3. Aprire con un editor di testo (Gedit, TextEdit, Blocco Note, VS Code, ...) il file `.env` presente nella cartella principale dell'applicativo e modificare gli indirizzi IP con quelli trovati al punto 2, poi salvare e chiudere.
 4. Cliccare sull'icona `launch` per lanciare l'applicativo.
 5. Semmai il punto 4 non dovesse funzionare, per eseguire l'applicativo occorre:
    - Aprire il Terminale
    - Entrare nella cartella ATEMTallyLights, utilizzando il comando `cd` per scorrere tra le directory e `ls` per visualizzare il contenuto della cartella ([video Tutorial](https://youtu.be/cPeC-GacLNA)).
    - Una volta entrati nella cartella, digitare `node index.js` e attendere l'avvio dell'applicativo.
 5. Verrà aperta una finestra di browser contenente un QRCode che ogni dispositivo dovrà scansionare per accedere all'applicativo.
 
E' possibile integrare ATEMTallyLights con ([NodeRestreamer](https://gitlab.com/lucapada/noderestreamer)) per visualizzare il flusso video nella stessa finestra in cui viene mostrato in tempo reale lo status del proprio input.

## Chiusura dell'applicativo

Per chiudere l'applicativo è sufficiente premere la combinazione di tasti `CTRL+C` su Windows o `CMD+C` nel Terminale, poi chiuderlo.

## Credits

 - [Dev1an/Atem: Implementation of BlackMagicDesign's ATEM v6.8
   communication protocol in javascript (written for nodejs).
   (github.com)](https://github.com/Dev1an/Atem) 
 - luca@alpad.it
