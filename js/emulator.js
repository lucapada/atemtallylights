var socket = io();

var app = angular.module("atemApp", []);
app.controller("atemCtrl", function($scope){
    $scope.channels = [];
    $scope.prw = 0;
    $scope.prg = 1;
    $scope.atem = function(){
        for(var i = 1; i <= parseInt($scope.numeroInput); i++){
            $scope.channels.push(i);
        }
    };

    $scope.preview = function(ch){
        if($scope.prg != ch){
            $scope.prw = ch;
            sendWS($scope.prw, $scope.prg);
        }
    };

    $scope.cut = function(){
        var exPrg = $scope.prg;
        $scope.prg = $scope.prw;
        $scope.prw = exPrg;

        sendWS($scope.prw, $scope.prg);
    };
});

function sendWS(prw, prg){
    console.log(prw, prg);
    var v = {PREVIEW: prw, PROGRAM: prg};
    socket.emit("emulatore", v);
}