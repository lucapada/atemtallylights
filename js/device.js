var input = parseInt(prompt("digitare l'input associato a questo dispositivo"));
var socket = io();

$(".pnlBG").attr("class", "connection pnlBG");
$(".pnlCTX").html("<h1>CONNECTION...</h1><h2><small>input: </small>" + input + "</h2>");

$(function(){
    socket.on('connect', (s) => {
        getAtemStatus();
    });

    socket.on('tally', (ev) => {
        parseAtemStatus(ev);
    });
});

function getAtemStatus(){
    $.ajax({
        url: './getAtemStatus',
        success: (data) => {
            parseAtemStatus(data);
        }
    });
}

function parseAtemStatus(atemStatus){
    if(atemStatus.PREVIEW == input || atemStatus.PROGRAM == input){
        // dove sono coinvolto?
        if(atemStatus.PREVIEW == input){
            console.log("PRW");
            //sono in preview: imposto a preview
            $(".pnlBG").attr("class", "preview pnlBG");
            $(".pnlCTX").html("<h1>PREVIEW</h1><h2><small>input: </small>" + input + "</h2><div class='fixed-bottom bg-secondary p-1'><span style='color: red;'>PROGRAM: " + atemStatus.PROGRAM + "</span></div>");
        } else if(atemStatus.PROGRAM == input){
            console.log("PRG");
            //sono in program: imposto a program
            $(".pnlBG").attr("class", "program pnlBG");
            $(".pnlCTX").html("<h1>PROGRAM</h1><h2><small>input: </small>" + input + "</h2><div class='fixed-bottom bg-secondary p-1'><span style='color: green;'>PREVIEW: " + atemStatus.PREVIEW + "</span></div>");
        }
    } else {
        console.log("NON");
        // non sono coinvolto: imposto a ready e faccio comparire la barra con chi sta in program e preview ora
        $(".pnlBG").attr("class", "ready pnlBG");
        $(".pnlCTX").html("<h1>READY</h1><h2><small>input: </small>" + input + "</h2><div class='fixed-bottom bg-secondary p-1'><span style='color: green;'>PREVIEW: " + atemStatus.PREVIEW + "</span> | <span style='color: red;'>PROGRAM: " + atemStatus.PROGRAM + "</span></div>");
    }
}